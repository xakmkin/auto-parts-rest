<?php

use yii\db\Migration;

/**
 * Class m210602_061216_insert_admin_data
 */
class m210602_061216_insert_admin_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->insert('admin', [
            'username' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210602_061216_insert_admin_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210602_060716_insert_admin_data cannot be reverted.\n";

        return false;
    }
    */
}
