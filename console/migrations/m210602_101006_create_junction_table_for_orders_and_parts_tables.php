<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders_parts}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%orders}}`
 * - `{{%parts}}`
 */
class m210602_101006_create_junction_table_for_orders_and_parts_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%orders_parts}}', [
            'orders_id' => $this->integer(),
            'parts_id' => $this->integer(),
            'count' => $this->integer(),
            'PRIMARY KEY(orders_id, parts_id)',
        ]);

        // creates index for column `orders_id`
        $this->createIndex(
            '{{%idx-orders_parts-orders_id}}',
            '{{%orders_parts}}',
            'orders_id'
        );

        // add foreign key for table `{{%orders}}`
        $this->addForeignKey(
            '{{%fk-orders_parts-orders_id}}',
            '{{%orders_parts}}',
            'orders_id',
            '{{%orders}}',
            'id',
            'CASCADE'
        );

        // creates index for column `parts_id`
        $this->createIndex(
            '{{%idx-orders_parts-parts_id}}',
            '{{%orders_parts}}',
            'parts_id'
        );

        // add foreign key for table `{{%parts}}`
        $this->addForeignKey(
            '{{%fk-orders_parts-parts_id}}',
            '{{%orders_parts}}',
            'parts_id',
            '{{%parts}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%orders}}`
        $this->dropForeignKey(
            '{{%fk-orders_parts-orders_id}}',
            '{{%orders_parts}}'
        );

        // drops index for column `orders_id`
        $this->dropIndex(
            '{{%idx-orders_parts-orders_id}}',
            '{{%orders_parts}}'
        );

        // drops foreign key for table `{{%parts}}`
        $this->dropForeignKey(
            '{{%fk-orders_parts-parts_id}}',
            '{{%orders_parts}}'
        );

        // drops index for column `parts_id`
        $this->dropIndex(
            '{{%idx-orders_parts-parts_id}}',
            '{{%orders_parts}}'
        );

        $this->dropTable('{{%orders_parts}}');
    }
}
