<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders_parts}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%orders}}`
 * - `{{%parts}}`
 */
class  m210602_161012_insert_data_to_tables extends Migration
{


    public function safeUp()
    {
    	//Users
        $this->insert('user', [
            'username' => 'user1',
            'auth_key' => 'abbccc',
            'status' => '10',
            'email' => 'user1@ema.il'
        ]);
        $this->insert('user', [
            'username' => 'user2',
            'auth_key' => 'aaabbc',
            'status' => '10',
            'email' => 'user2@ema.il'
        ]);
        $this->insert('user', [
            'username' => 'user3',
            'auth_key' => 'abcabc',
            'status' => '10',
            'email' => 'user3@ema.il'
        ]);

        //Parts
        $this->insert('parts', [
        	'id' => '1',
            'name' => 'Part1',
            'number' => '111111-c',
            'count' => '10',
            'price' => '2000'
        ]);
        $this->insert('parts', [
        	'id' => '2',
            'name' => 'Part2',
            'number' => '22222',
            'count' => '12',
            'price' => '3000'
        ]);
        $this->insert('parts', [
        	'id' => '3',
            'name' => 'Part3',
            'number' => '564343253',
            'count' => '2400',
            'price' => '440'
        ]);
        $this->insert('parts', [
        	'id' => '4',
            'name' => 'Part4',
            'number' => '544-CNB-24',
            'count' => '250',
            'price' => '200'
        ]);

        //Orders
        $this->insert('orders', [
        	'id' => '1',
            'user_id' => '1',
            'create_date' => '1622635014',
            'edit_date' => '1622635014',
            'status' => '0'
        ]);
        $this->insert('orders', [
        	'id' => '2',
            'user_id' => '2',
            'create_date' => '1622635014',
            'edit_date' => '1622635014',
            'status' => '0'
        ]);


        //OrdersParts
        $this->insert('orders_parts', [
        	'orders_id' => '1',
            'parts_id' => '1',
            'count' => '1',
        ]);
        $this->insert('orders_parts', [
        	'orders_id' => '1',
            'parts_id' => '3',
            'count' => '4',
        ]);
        $this->insert('orders_parts', [
        	'orders_id' => '2',
            'parts_id' => '4',
            'count' => '3',
        ]);

    }

    public function safeDown()
    {
    	$this->truncateTable('user');
    	$this->truncateTable('parts');
    	$this->truncateTable('orders');
    	$this->truncateTable('orders_parts');
    }
}