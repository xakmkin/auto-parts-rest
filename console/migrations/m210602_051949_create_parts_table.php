<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%parts}}`.
 */
class m210602_051949_create_parts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%parts}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'number' => $this->string(255),
            'count' => $this->integer(),
            'price' => $this->float(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%parts}}');
    }
}
