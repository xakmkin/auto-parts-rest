<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use yii\data\ActiveDataProvider;

/**
 * Site controller
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [];
    }

    /**
     * Displays users.
     *
     * @return string
     */
    public function actionIndex()
    {
        $query = User::find();
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);


        return $this->render('index',[
            'users' => $provider
        ]);
    }


    /**
     * View action.
     *
     * @return string
     */
    public function actionView()
    {
        $user = User::find()->where(['id' => Yii::$app->request->get('id')])->one();
        return $this->render('view',[
            'user' => $user
        ]);
    }

    public function actionUpdate()
    {
        $user = User::find()->where(['id' => Yii::$app->request->get('id')])->one();

        if($user->load(Yii::$app->request->post()) && $user->save())
        {
            return $this->redirect(['index']);
        }

        return $this->render('edit',[
            'user' => $user
        ]);
    }

    public function actionCreate()
    {
        $user = new User();

        if($user->load(Yii::$app->request->post()) && $user->save())
        {
            return $this->redirect(['index']);
        }

        return $this->render('edit',[
            'user' => $user
        ]);
    }

    public function actionDelete()
    {
        $user = User::find()->where(['id' => Yii::$app->request->get('id')])->one();
        $user->delete();

        return $this->redirect(['index']);
    }
}
