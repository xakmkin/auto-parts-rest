<?php
namespace api\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use api\models\LoginForm;
use api\models\Parts;
use api\models\Orders;
use common\models\User;

use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;


/**
 * Site controller
 */
class SiteController extends ActiveController
{
    public $modelClass = 'common\models\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                HttpBasicAuth::class,
                HttpBearerAuth::class,
                QueryParamAuth::class,
            ],
        ];
        return $behaviors;
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionIndex()
    {
        $query = Parts::find()->select('id, name, number');
        if(Yii::$app->request->get('name')){
            $query->andWhere(['like', 'name', Yii::$app->request->get('name')]);
        }
        if(Yii::$app->request->get('number')){
            $query->andWhere(['like', 'number', Yii::$app->request->get('number')]);
        }
        if(Yii::$app->request->get('price')){
            $query->andWhere(['like', 'price', Yii::$app->request->get('price')]);
        }
        if(Yii::$app->request->get('count')){
            $query->andWhere(['like', 'count', Yii::$app->request->get('count')]);
        }

        if(!Yii::$app->request->get('number') && !Yii::$app->request->get('name')){
            return Parts::find()->count();
        }
        else{
            return $query->all(); 
        }
    }

    public function actionView()
    {
        if(Yii::$app->request->get('id')){
            return Parts::find()->where(['id' => Yii::$app->request->get('id')])->one();
        }
        return null;
    }

    public function actionShowOrder()
    {
        $order = Orders::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
        if($order) {
            return $order->getOrder();
        }
        return null;
    }

    public function actionAddOrder()
    {
        $order = Orders::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
        if(!$order) {
            $order = new Orders();
            $order->user_id = Yii::$app->user->identity->id;
            $order->create_date = time();
            $order->save();
        }

        if(Yii::$app->request->get('id')) {
            $id = Yii::$app->request->get('id');
            Yii::$app->request->get('count') ?
                $count = Yii::$app->request->get('count') :
                $count = 1;

            if($order_part = $order->checkPart($id)) {
                $return = $order->addPart($order_part, $id, Yii::$app->request->get('count'));
            }
            else {
                $return = $order->newPart($id, $count);
            }

            $order->edit_date = time();
            $order->save();

            return $return;
        }
        return false;
    }

    // public function actionRemoveOrder()
    // {
    //     //  очистить корзину
    //     $order = Orders::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
    //     $order->removeOrder();
    // }
}
