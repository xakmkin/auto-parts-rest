<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "parts".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $number
 * @property int|null $count
 * @property float|null $price
 */
class Parts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['count'], 'integer'],
            [['price'], 'number'],
            [['name', 'number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'number' => 'Number',
            'count' => 'Count',
            'price' => 'Price',
        ];
    }
}
