<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $create_date
 * @property int|null $edit_date
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'create_date', 'edit_date'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'create_date' => 'Create Date',
            'edit_date' => 'Edit Date',
        ];
    }


    public function getOrder()
    {
        return OrdersParts::find()
            ->select('parts.*, orders_parts.count')
            ->leftJoin('parts', 'parts.id = orders_parts.parts_id')
            ->where(['orders_id' => $this->id])
            ->asArray()->all();
    }
    

    public function checkPart($id)
    {
        return OrdersParts::find()
            ->where(['orders_id' => $this->id])
            ->andWhere(['parts_id' => $id])
            ->one();
    }

    public function addPart($order_part, $id, $count)
    {
        $order_part->count += $count;
        return $order_part->save() ? true : false;
    }

    public function newPart($id, $count)
    {
        $order_part = new OrdersParts();
        $order_part->count = $count;
        $order_part->parts_id = $id;
        $order_part->orders_id = $this->id;
        return $order_part->save() ? true : false;
    }

    // public function removeOrder(){
    //     $order_part = OrdersParts::find()
    //         ->where(['orders_id' => $this->id]);
    //     $order_part->delete();
    //     $this->delete();
    // }
      
}
