<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "orders_parts".
 *
 * @property int $orders_id
 * @property int $parts_id
 * @property int|null $count
 */
class OrdersParts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders_parts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orders_id', 'parts_id'], 'required'],
            [['orders_id', 'parts_id', 'count'], 'integer'],
            [['orders_id', 'parts_id'], 'unique', 'targetAttribute' => ['orders_id', 'parts_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'orders_id' => 'Orders ID',
            'parts_id' => 'Parts ID',
            'count' => 'Count',
        ];
    }

    public function getParts(){
        return $this->hasMany(Parts::class, ['part_id' => 'id']);
    }
}
